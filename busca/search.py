# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    from game import Directions

    pilha_estados = util.Stack() #pilha para ler todos os estados LIFO
    estados_visitados = [] #Array com todos os estados ja visitados, para que nao entre em loop
    caminho = [] #Array que retornara o caminho utilizado

    pilha_estados.push((problem.getStartState(),caminho))

    while not pilha_estados.isEmpty():
        estado = pilha_estados.pop()
        direcao = estado[1]
        estado = estado[0]

        if problem.isGoalState(estado):
            return direcao

        if not estado in estados_visitados: #se o estado nao foi visitado
            estados_visitados.append(estado)
            vizinhos = problem.getSuccessors(estado);

            for x in xrange(0,len(vizinhos)):
                pilha_estados.push((vizinhos[x][0],direcao+[vizinhos[x][1]]))


def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""

    """Para este algoritmo foi utilizada a mesma logica do anterior, na busca em profundidade, porem 
    utilizamos uma estrutura FIFO, dessa forma quanto antes o no e visitado mais rapido ele sera testado"""

    import Queue
    from game import Directions

    pilha_estados = Queue.Queue() #fila para ler todos os estados FIFO
    estados_visitados = [] #Array com todos os estados ja visitados, para que nao entre em loop
    caminho = [] #Array que retornara o caminho utilizado

    pilha_estados.put((problem.getStartState(),caminho))

    while not pilha_estados.empty():
        estado = pilha_estados.get()
        direcao = estado[1]
        estado = estado[0]

        if problem.isGoalState(estado):
                return direcao

        if not estado in estados_visitados: #se o estado nao foi visitado
            estados_visitados.append(estado)
            vizinhos = problem.getSuccessors(estado);

            for x in xrange(0,len(vizinhos)):
                pilha_estados.put((vizinhos[x][0],direcao+[vizinhos[x][1]]))

def iterativeDeepeningSearch(problem):
    """
    Start with depth = 0.
    Search the deepest nodes in the search tree first up to a given depth.
    If solution not found, increment depth limit and start over.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.
    """
    from game import Directions

    depth = 0
    in_depth = -1
   
    while in_depth <= depth:
        pilha_estados = util.Stack() #pilha para ler todos os estados LIFO
        estados_visitados = [] #Array com todos os estados ja visitados, para que nao entre em loop
        caminho = [] #Array que retornara o caminho utilizado

        in_depth = -1
        pilha_estados.push((problem.getStartState(),caminho,0))

        while not pilha_estados.isEmpty():
            estado = pilha_estados.pop()
            in_depth = estado[2]
            direcao = estado[1]
            estado = estado[0]

            if problem.isGoalState(estado):
                return direcao

            if not estado in estados_visitados: #se o estado nao foi visitado
                estados_visitados.append(estado)
                vizinhos = problem.getSuccessors(estado);

                for x in xrange(0,len(vizinhos)):
                    pilha_estados.push((vizinhos[x][0],direcao+[vizinhos[x][1]],in_depth+1))
            depth = depth + 1

    return caminho

def uniformCostSearch(problem):
    """Search the node of least total cost first."""

    from util import PriorityQueue
    from game import Directions
    
    #baseado em https://algorithmicthoughts.wordpress.com/2012/12/15/artificial-intelligence-uniform-cost-searchucs/
    
    pilha_estados = PriorityQueue()
    estados_visitados = [] #Array com todos os estados ja visitados, para que nao entre em loop
    caminho = [] #Array que retornara o caminho utilizado

    start_state = problem.getStartState();
    pilha_estados.push((start_state,caminho,0),0)

    while not pilha_estados.isEmpty():
        #remover o elemento de maior prioridade
        estado = pilha_estados.pop()
        direcao = estado[1]
        custo = estado[2]
        estado = estado[0]

        if problem.isGoalState(estado):
            return direcao

        if not estado in estados_visitados: #se o estado nao foi visitado
            estados_visitados.append(estado)
            vizinhos = problem.getSuccessors(estado);
            for x in xrange(0,len(vizinhos)):
                    pilha_estados.push((vizinhos[x][0],direcao+[vizinhos[x][1]],vizinhos[x][2]+custo), vizinhos[x][2]+custo)


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
ids = iterativeDeepeningSearch